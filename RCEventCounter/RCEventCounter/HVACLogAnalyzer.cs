﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RC.CodingChallenge {
    public class HVACLogAnalyzer : IEventCounter {
        static void Main(string[] args) {
            var test = new HVACLogAnalyzer();
            test.GenerateTestData("TestFile");
            Console.WriteLine("Faults - " + test.GetEventCount("TestFile"));
            Console.ReadLine();
        }

        public void ParseEvents(string deviceID, StreamReader eventLog) {
            // I changed the signature of this function because as it is, this doesn't make sense to me. 
            // In order for this function to make sense, it would either have to count all the events and return the number 
            //      (which GetEventCount should do, and this has no return value)
            // or populate the event log stream reader to be used by reference.

            // if we populate the stream reader here, then no actual parsing is happening, and also the reader would have to be
            // pupulated in it's entirety.  (no buffering or chunked reading could happen)

            // so, I figure this function should do what it's name suggests, and actually do the parsing bit
        }

        /// <summary>
        /// Parse a byte array into HVACEvents. 
        /// </summary>
        /// <param name="bytes">Array of characters to parse</param>
        /// <param name="records">optional - If the byte array is not full (buffer size will always be the size of the data) then specify how many records to check</param>
        /// <returns></returns>
        public List<HVACLogEvent> ParseEvents(char[] bytes, int records = 0) {
            // There would be much faster ways to search specifically for the error we want
            // but this function should only do what it says it does.  It could be useful
            // to have a list of events for many reasons, not just searching for this particular series of events
            // that's why I've chosen to use Lists and do this generically

            List<HVACLogEvent> events = new List<HVACLogEvent>();
            long numRecords = records == 0 ? bytes.Length / HVACLogEvent.RECORD_SIZE_IN_BYTES : records;
            StringBuilder dateString = new StringBuilder();
            int state = 0;

            for (int i = 0; i < numRecords; i++) {
                // date time string is always 19 characters
                for (int d = 0; d < 19; d++) {
                    dateString.Append(bytes[i * HVACLogEvent.RECORD_SIZE_IN_BYTES + d]);
                }
                state = (int)Char.GetNumericValue(bytes[i * HVACLogEvent.RECORD_SIZE_IN_BYTES + 20]); // state is always index 20
                events.Add(new HVACLogEvent(DateTime.Parse(dateString.ToString()), state));
                dateString.Clear();
            }

            return events;
        }


        /// <summary>
        /// Gets the total number of faults in an entire log file
        /// </summary>
        /// <param name="deviceID">file name of the Log</param>
        /// <returns>int - the total number of faults</returns>
        public int GetEventCount(string deviceID) {
            string path = $"{System.Environment.CurrentDirectory}/Data/{deviceID}.csv";
            int faultEvents = 0;
            int recordsPerRead = 255;
            int bufferSize = recordsPerRead * HVACLogEvent.RECORD_SIZE_IN_BYTES;

            char[] curBytes = new char[bufferSize];
            StreamReader reader = new StreamReader(path, Encoding.UTF8, false, bufferSize);
            long fileLength = new FileInfo(path).Length;
            long fullReads = fileLength / bufferSize; // how many reads will be full buffers
            int remainder = 0;

            if (fileLength > bufferSize) {
                remainder = (int)(fileLength % (fullReads * bufferSize) / HVACLogEvent.RECORD_SIZE_IN_BYTES);
            }
            else {
                remainder = (int)fileLength / HVACLogEvent.RECORD_SIZE_IN_BYTES;
            }

            long timesRead = 0;
            // since we are reading records in batches, we will have to keep a few of the previous batch sometimes
            // because the final record of the first batch may be a flaw, but if we don't compare it to the second batch
            // we will never know
            List<HVACLogEvent> parsedRecords = new List<HVACLogEvent>();    // currently parsed batch
            List<HVACLogEvent> rolloverRecords = new List<HVACLogEvent>();  // rollover from previous batch
            List<HVACLogEvent> allRecords = new List<HVACLogEvent>();       // for clarity of reading, store both in a variable

            while (!reader.EndOfStream) {
                lock (this) {
                    curBytes = new char[bufferSize]; // clear the bytes
                    timesRead++;
                    reader.ReadBlock(curBytes, 0, bufferSize);
                    if (timesRead <= fullReads) {
                        parsedRecords = ParseEvents(curBytes);
                    }
                    else {
                        parsedRecords = ParseEvents(curBytes, remainder);
                    }
                    allRecords = new List<HVACLogEvent>();
                    allRecords.AddRange(rolloverRecords);
                    allRecords.AddRange(parsedRecords);
                    faultEvents += CountFaultEvents(allRecords);

                    rolloverRecords = FindRolloverRecords(parsedRecords);
                }
            }

            return faultEvents;
        }

        /// <summary>
        /// Count the fault occurences in a list of events
        /// A fault happens when these things happen in order in the log:
        /// 
        /// 1) stage 3 for 5 or more minutes
        /// 2) stage 2
        /// 3) any number of changes from stage 3 and 2 (including zero)
        /// 4) stage 0 
        /// </summary>
        /// <param name="events">List of events to be checked</param>
        /// <returns>int - the number of faults in the list</returns>
        public int CountFaultEvents(List<HVACLogEvent> events) {
            int faults = 0;

            // find all times when a state 3 is followed by a state 2.
            var stage3to2 = events.Where(x => x.state == 3 && events.IndexOf(x) != events.Count - 1 && events[events.IndexOf(x) + 1].state == 2);
            // from the prior list, find if stage 3 was more than 5 minutes
            var withinTimeLimit = stage3to2.Where(x => Math.Abs(x.dateTime.Subtract(events[events.IndexOf(x) + 1].dateTime).Minutes) >= 5);

            // everything matching the prior criteria needs to be checked to see if there are any state 1 before a state 0 happens
            // if so, there is no fault here
            foreach (var item in withinTimeLimit) {
                // find the closest state 0, and check all inbetween for a state 1 (state 2 and 3 are fine)
                var nearestStage0 = events.Where(x => x.state == 0 && events.IndexOf(x) > events.IndexOf(item)).OrderBy(x => events.IndexOf(x)).FirstOrDefault();
                if (nearestStage0 != null) {
                    var state1 = events.Where(x => events.IndexOf(x) > events.IndexOf(item) && events.IndexOf(x) < events.IndexOf(nearestStage0) && x.state == 1).Count();
                    if (state1 > 0) {
                        // a fault occurred here
                        faults++;
                    }
                }
            }
            return faults;
        }


        /// <summary>
        /// Check for the last records that we need to keep for our next buffered search
        /// </summary>
        /// <param name="oldList">The last list of events that were parsed</param>
        /// <returns>a trimmed version of oldList containing only the ones we need to re-check</returns>
        List<HVACLogEvent> FindRolloverRecords(List<HVACLogEvent> oldList) {
            var newList = new List<HVACLogEvent>();
            // find a change from 3 to 2, or if 3 is the final state in the buffer. the last possible instance of either is what we want
            // we could be more certain (like if we find a state 1, we could drop it) but it's not really that much of a gain for the complexity.
            // if we check the same record twice, it's not a huge deal
            var lastPossibleStartingPoint = oldList.Where(x => x.state == 3 || oldList.IndexOf(x) == oldList.Count - 1 || (oldList[oldList.IndexOf(x) + 1].state == 2)).OrderBy(x => oldList.IndexOf(x)).LastOrDefault();
            if (lastPossibleStartingPoint != null) {
                // add the last possible record, plus any that are after it
                newList.AddRange(oldList.Where(x => oldList.IndexOf(x) >= oldList.IndexOf(lastPossibleStartingPoint)));
            }
            return newList;
        }


        /// <summary>
        /// Creates a bunch of random records to test with
        /// </summary>
        /// <param name="deviceID">File name of the Log</param>
        public void GenerateTestData(string deviceID) {

            DateTime curDT = DateTime.Now.Subtract(TimeSpan.FromDays(5));// start at a given amount of time less than today 
            int state = 0;
            Random rand = new Random();
            string path = $"{System.Environment.CurrentDirectory}/Data/{deviceID}.csv";
            StreamWriter writer;

            // Create test data directory
            if (!Directory.Exists($"{System.Environment.CurrentDirectory}/Data")) {
                Directory.CreateDirectory($"{System.Environment.CurrentDirectory}/Data");
            }

            // Create or retrieve file
            if (!File.Exists(path)) {
                writer = File.CreateText(path);
            }
            else {
                writer = new StreamWriter(path, false, Encoding.UTF8);
            }

            // populate a bunch of records with random time intervals
            do {
                writer.Write($"{curDT.ToString("yyyy-MM-dd HH:mm:ss")},{state},");
                curDT = curDT.AddMinutes(rand.Next(0, 11));
                curDT = curDT.AddSeconds(rand.Next(0, 60));
                state = rand.Next(0, 4);
            } while (curDT < DateTime.Now);

            // ensure stream buffer is finished writing
            writer.Flush();
            writer.Close();

        }
    }
}
