﻿//-----------------------------------------------------------------------------
// <copyright file="EventCounter.cs" company="Reliable Controls Corporation">
//   Copyright (C) Reliable Controls Corporation.  All rights reserved.
// </copyright>
//
// Description:
//      A service that identifies and counts events which are associated with a
//      specific sequence of operations.
//
//      The input logs are comprised of lines of text that indicate the time of a
//      recording, and the value recorded
//
//      1998-03-07 06:25:32	2
//      1998-03-07 09:15:55	3
//      1998-03-07 12:00:02	3
//      1998-03-07 14:28:27	0
//
//      The columns (1) date+time in ISO-8601 format, (2) value indicating HVAC
//      unit stage by tabs.
//
//
// Edit - William Prevett Feb 2018
//
//-----------------------------------------------------------------------------
namespace RC.CodingChallenge {
    using System.IO;
    using System.Collections.Generic;
    public interface IEventCounter {
        
        /// <summary>
        /// Parse a char array into a list of HVACLogEvents for analysis, or other use
        /// </summary>
        /// <param name="bytes">A block of characters (probably from a StreamReader)</param>
        /// <param name="records">Optional - if you are doing buffered reads, the char array may be larger than your data set, so specify records to read</param>
        /// <returns></returns>
        List<HVACLogEvent> ParseEvents(char[] bytes, int records);

        /// <summary>
        /// Gets the current count of events detected for the given device
        /// </summary>
        /// <returns>An integer representing the number of detected events</returns>
        int GetEventCount(string deviceID);
    }
}