﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RC.CodingChallenge {
    public class HVACLogEvent {
        // This could be dynamically determined, and probably should but since we are
        // formatting dates in a particular way, and our states are always one digit
        // we know exactly how long each record will be.  So I'm saving myself some time here   :S
        public const int RECORD_SIZE_IN_BYTES = 22;

        public DateTime dateTime;
        public int state;

        public HVACLogEvent(DateTime dateTime, int state) {
            this.dateTime = dateTime;
            this.state = state;
        }
    }
}
