﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RC.CodingChallenge;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventCounterTests {
    [TestClass]
    public class HVACLogAnalyzerTest {

        char[] CreateTestData() {
            var times = new DateTime[] {
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(0)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(6)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(2)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(3)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(10)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(3)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(8)),
            };

            var states = new int[] { 3, 2, 1, 0, 2, 1, 3 };
            StringBuilder str = new StringBuilder();

            for (int i = 0; i < times.Length; i++) {
                str.Append($"{times[i].ToString("yyyy-MM-dd HH:mm:ss")},{states[i]},");
            }
            var testData = str.ToString().ToCharArray();
            return testData;
        }

        char[] CreateTestDataWithFaults(int faults) {
            var times = new DateTime[] {
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(0)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(6)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(2)),
                DateTime.Now.Subtract(TimeSpan.FromMinutes(60)).Add(TimeSpan.FromMinutes(4))
            };

            var states = new int[] { 3, 2, 1, 0 };
            StringBuilder str = new StringBuilder();

            if (faults == 0) { faults = 1; } // just in case
            
            for (int i = 0; i < faults; i++) {
                for(int j = 0; j < 4; j++) { 
                    str.Append($"{times[j].ToString("yyyy-MM-dd HH:mm:ss")},{states[j]},");
                }
            }
            var testData = str.ToString().ToCharArray();
            return testData;
        }

        [TestMethod]
        void GenerateTestData_NoExceptions() {
            var analyzer = new HVACLogAnalyzer();
            analyzer.GenerateTestData("TestFile");
        }


        [TestMethod]
        public void ParseEvents_NumberOfRecordsCorrect() {
            var analyzer = new HVACLogAnalyzer();
            var list = analyzer.ParseEvents(CreateTestData());

            Assert.AreEqual(list.Count, 7);
        }

        [TestMethod]
        public void ParseEvents_DataTypeCorrect() {
            var analyzer = new HVACLogAnalyzer();
            var list = analyzer.ParseEvents(CreateTestData());

            Assert.IsInstanceOfType(list, typeof(List<HVACLogEvent>));
        }

        [TestMethod]
        public void CountFaults_NoExceptions() {
            var analyzer = new HVACLogAnalyzer();
            analyzer.GetEventCount("TestFile");
        }

        [TestMethod]
        public void GetEventCount_MultiThreaded_OneAnalyzer(){
            int numThreads = 100;
            var analyzer = new HVACLogAnalyzer();

            for (int i = 0; i < numThreads; i++) {
                var thread = Task.Factory.StartNew(() => {
                    analyzer.GetEventCount("TestFile");
                });
            }
        }

        [TestMethod]
        public void GetEventCount_MultiThreaded_ManyAnalyzers() {
            int numThreads = 100;
            
            for (int i = 0; i < numThreads; i++) {
                var thread = Task.Factory.StartNew(() => {
                    var analyzer = new HVACLogAnalyzer();
                    analyzer.GetEventCount("TestFile");
                });
            }
        }

        [TestMethod]
        public void CountFaultEvents_CorrectCount() {
            var analyzer = new HVACLogAnalyzer();
            var listFaults = analyzer.ParseEvents(CreateTestDataWithFaults(2));
            int count = analyzer.CountFaultEvents(listFaults);
            Assert.AreEqual(count, 2);
            listFaults = analyzer.ParseEvents(CreateTestDataWithFaults(4));
            count = analyzer.CountFaultEvents(listFaults);
            Assert.AreEqual(count, 4);
            listFaults = analyzer.ParseEvents(CreateTestDataWithFaults(1));
            count = analyzer.CountFaultEvents(listFaults);
            Assert.AreEqual(count, 1);
            listFaults = analyzer.ParseEvents(CreateTestDataWithFaults(10));
            count = analyzer.CountFaultEvents(listFaults);
            Assert.AreEqual(count, 10);
            listFaults = analyzer.ParseEvents(CreateTestDataWithFaults(25));
            count = analyzer.CountFaultEvents(listFaults);
            Assert.AreEqual(count, 25);
        }
    }
}
